<?php
/**
 * @file
 * openaccounting.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function openaccounting_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'master_panel';
  $page->task = 'page';
  $page->admin_title = 'Master Panel';
  $page->admin_description = 'The master panel view.';
  $page->path = 'megadash';
  $page->access = array();
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'weight' => '0',
    'name' => 'navigation',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
      'name' => 'navigation',
    ),
  );
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_master_panel_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'master_panel';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => FALSE,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Dashboard';
  $display->uuid = 'e1bcc7d5-4e92-4740-b8b4-c660286bd8c7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-29eef540-5555-43cb-b8e1-8a805f50bd5f';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'sales_dashboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => 'Sales',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '29eef540-5555-43cb-b8e1-8a805f50bd5f';
    $display->content['new-29eef540-5555-43cb-b8e1-8a805f50bd5f'] = $pane;
    $display->panels['middle'][0] = 'new-29eef540-5555-43cb-b8e1-8a805f50bd5f';
    $pane = new stdClass();
    $pane->pid = 'new-b262d8bc-8e6e-4b29-91b0-5a4cd1f5295c';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'discounted_sales_dashboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => 'Discounted Sales',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b262d8bc-8e6e-4b29-91b0-5a4cd1f5295c';
    $display->content['new-b262d8bc-8e6e-4b29-91b0-5a4cd1f5295c'] = $pane;
    $display->panels['middle'][1] = 'new-b262d8bc-8e6e-4b29-91b0-5a4cd1f5295c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-29eef540-5555-43cb-b8e1-8a805f50bd5f';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['master_panel'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'shipping_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Shipping Dashboard';
  $page->admin_description = 'Shipping master panel.';
  $page->path = 'shipping';
  $page->access = array();
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'weight' => '0',
    'name' => 'navigation',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
      'name' => 'navigation',
    ),
  );
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_shipping_dashboard_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'shipping_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => FALSE,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Shipping Dashboard';
  $display->uuid = '34e05188-5b58-4a45-a31a-970990a58f9c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2ec01eaa-cba3-41b8-a7c0-2a2d4b01565a';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'shipping_dashboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => 'Shipping',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ec01eaa-cba3-41b8-a7c0-2a2d4b01565a';
    $display->content['new-2ec01eaa-cba3-41b8-a7c0-2a2d4b01565a'] = $pane;
    $display->panels['middle'][0] = 'new-2ec01eaa-cba3-41b8-a7c0-2a2d4b01565a';
    $pane = new stdClass();
    $pane->pid = 'new-3619e7f8-42db-4c8e-8b56-56bcfbfe3267';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'discounted_shipping_dashboard';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => 'Discounted Shipping',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3619e7f8-42db-4c8e-8b56-56bcfbfe3267';
    $display->content['new-3619e7f8-42db-4c8e-8b56-56bcfbfe3267'] = $pane;
    $display->panels['middle'][1] = 'new-3619e7f8-42db-4c8e-8b56-56bcfbfe3267';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-2ec01eaa-cba3-41b8-a7c0-2a2d4b01565a';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['shipping_dashboard'] = $page;

  return $pages;

}
