<?php
/**
 * @file
 * openaccounting.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function openaccounting_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-accounting.
  $menus['menu-accounting'] = array(
    'menu_name' => 'menu-accounting',
    'title' => 'Journal',
    'description' => 'This menu is where all the accounting options are displayed.',
  );
  // Exported menu: menu-dashboard.
  $menus['menu-dashboard'] = array(
    'menu_name' => 'menu-dashboard',
    'title' => 'Dashboard',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Dashboard');
  t('Journal');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('This menu is where all the accounting options are displayed.');


  return $menus;
}
