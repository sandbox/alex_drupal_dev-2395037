<?php
/**
 * @file
 * openaccounting.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function openaccounting_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create discounted_sale content'.
  $permissions['create discounted_sale content'] = array(
    'name' => 'create discounted_sale content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create journal_entries content'.
  $permissions['create journal_entries content'] = array(
    'name' => 'create journal_entries content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create sales_transaction content'.
  $permissions['create sales_transaction content'] = array(
    'name' => 'create sales_transaction content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any discounted_sale content'.
  $permissions['delete any discounted_sale content'] = array(
    'name' => 'delete any discounted_sale content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any journal_entries content'.
  $permissions['delete any journal_entries content'] = array(
    'name' => 'delete any journal_entries content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any sales_transaction content'.
  $permissions['delete any sales_transaction content'] = array(
    'name' => 'delete any sales_transaction content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own discounted_sale content'.
  $permissions['delete own discounted_sale content'] = array(
    'name' => 'delete own discounted_sale content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own journal_entries content'.
  $permissions['delete own journal_entries content'] = array(
    'name' => 'delete own journal_entries content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own sales_transaction content'.
  $permissions['delete own sales_transaction content'] = array(
    'name' => 'delete own sales_transaction content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any discounted_sale content'.
  $permissions['edit any discounted_sale content'] = array(
    'name' => 'edit any discounted_sale content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any journal_entries content'.
  $permissions['edit any journal_entries content'] = array(
    'name' => 'edit any journal_entries content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any sales_transaction content'.
  $permissions['edit any sales_transaction content'] = array(
    'name' => 'edit any sales_transaction content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own discounted_sale content'.
  $permissions['edit own discounted_sale content'] = array(
    'name' => 'edit own discounted_sale content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own journal_entries content'.
  $permissions['edit own journal_entries content'] = array(
    'name' => 'edit own journal_entries content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own sales_transaction content'.
  $permissions['edit own sales_transaction content'] = array(
    'name' => 'edit own sales_transaction content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
