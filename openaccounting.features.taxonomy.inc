<?php
/**
 * @file
 * openaccounting.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function openaccounting_taxonomy_default_vocabularies() {
  return array(
    'accounts' => array(
      'name' => 'Accounts',
      'machine_name' => 'accounts',
      'description' => 'Accounts available for journal entry.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
