<?php
/**
 * @file
 * openaccounting.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openaccounting_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function openaccounting_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openaccounting_node_info() {
  $items = array(
    'discounted_sale' => array(
      'name' => t('Discounted Sale'),
      'base' => 'node_content',
      'description' => t('Add a new discounted sale to the accounting system.'),
      'has_title' => '1',
      'title_label' => t('Sale ID'),
      'help' => '',
    ),
    'journal_entries' => array(
      'name' => t('Journal Entries'),
      'base' => 'node_content',
      'description' => t('Add a journal entry into the accounting system.'),
      'has_title' => '1',
      'title_label' => t('Transaction'),
      'help' => t('Please describe this transaction.'),
    ),
    'sales_transaction' => array(
      'name' => t('Sales Transaction'),
      'base' => 'node_content',
      'description' => t('This adds a new sales transaction to the accounting system.'),
      'has_title' => '1',
      'title_label' => t('Sales ID'),
      'help' => t('Please create a unique sales ID number or phrase to identify this transaction.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
