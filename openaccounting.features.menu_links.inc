<?php
/**
 * @file
 * openaccounting.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function openaccounting_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_place-discounted-order:node/add/discounted-sale
  $menu_links['main-menu_place-discounted-order:node/add/discounted-sale'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/discounted-sale',
    'router_path' => 'node/add/discounted-sale',
    'link_title' => 'Place Discounted Order',
    'options' => array(
      'attributes' => array(
        'title' => 'This will add a new discounted sale to the website. This is used for wholesale customers or other customers with a discounted rate.',
      ),
      'identifier' => 'main-menu_place-discounted-order:node/add/discounted-sale',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_place-order:node/add/sales-transaction
  $menu_links['main-menu_place-order:node/add/sales-transaction'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/sales-transaction',
    'router_path' => 'node/add/sales-transaction',
    'link_title' => 'Place Order',
    'options' => array(
      'attributes' => array(
        'title' => 'Add a new sale transaction to the accounting system.',
      ),
      'identifier' => 'main-menu_place-order:node/add/sales-transaction',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-accounting_add-a-journal-entry:node/add/journal-entries
  $menu_links['menu-accounting_add-a-journal-entry:node/add/journal-entries'] = array(
    'menu_name' => 'menu-accounting',
    'link_path' => 'node/add/journal-entries',
    'router_path' => 'node/add/journal-entries',
    'link_title' => 'Add a journal entry.',
    'options' => array(
      'attributes' => array(
        'title' => 'You can use this link to add a new journal entry to the accounting system.',
      ),
      'identifier' => 'menu-accounting_add-a-journal-entry:node/add/journal-entries',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-accounting_audit-journal-entries:journal-entries
  $menu_links['menu-accounting_audit-journal-entries:journal-entries'] = array(
    'menu_name' => 'menu-accounting',
    'link_path' => 'journal-entries',
    'router_path' => 'journal-entries',
    'link_title' => 'Audit journal entries.',
    'options' => array(
      'attributes' => array(
        'title' => 'Look at the most recent journal entries.',
      ),
      'identifier' => 'menu-accounting_audit-journal-entries:journal-entries',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-dashboard_sales-dashboard:megadash
  $menu_links['menu-dashboard_sales-dashboard:megadash'] = array(
    'menu_name' => 'menu-dashboard',
    'link_path' => 'megadash',
    'router_path' => 'megadash',
    'link_title' => 'Sales dashboard.',
    'options' => array(
      'attributes' => array(
        'title' => 'This brings you to your most recent sales.',
      ),
      'identifier' => 'menu-dashboard_sales-dashboard:megadash',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-dashboard_shipping-dashboard:shipping
  $menu_links['menu-dashboard_shipping-dashboard:shipping'] = array(
    'menu_name' => 'menu-dashboard',
    'link_path' => 'shipping',
    'router_path' => 'shipping',
    'link_title' => 'Shipping dashboard.',
    'options' => array(
      'attributes' => array(
        'title' => 'This brings you to your shipping dashboard.',
      ),
      'identifier' => 'menu-dashboard_shipping-dashboard:shipping',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add a journal entry.');
  t('Audit journal entries.');
  t('Home');
  t('Place Discounted Order');
  t('Place Order');
  t('Sales dashboard.');
  t('Shipping dashboard.');


  return $menu_links;
}
